Packaged-By:
 This package was put together by Christian Kesselheim <ckesselh@debian.org>
 It was later maintained by Martin A. Godisch <godisch@debian.org>,
 then by Christoph Berg <myon@debian.org>.
 The current Debian maintainer is Bernhard R. Link <brlink@debian.org>.

Upstream-Maintainer: Markus Steinborn <msteinbo@uni-paderborn.de>
Original-Source-Location: ftp://ftp.gnu.org/gnu/gv/
Original-Source-Location: ftp://alpha.gnu.org/gnu/gv/

Copyright (C) 1992-1997 Johannes Plass <plass@thep.physik.uni-mainz.de>,
Department of Physics, Johannes Gutenberg-University, Mainz, Germany.
Copyright (C) 1992-1997 Timothy O. Theisen
Copyright (C) 2002 Olaf Kirch <okir@suse.de>
Copyright (C) 2004-2007 José E. Marchesi
Copyright (C) 2007-2010 Markus Steinborn <gnugv_maintainer@yahoo.de>
Copyright (C) 2008-2011 Bernhard R. Link
Copyright (C) 2010 Marco Paolone <marcopaolone@gmail.com>
Copyright (C) 2010 Quentin Gibeaux <quentin.gibeaux@member.fsf.org>
Copyright (C) 2011 César Gil <bluemoon.cgp@gmail.com>
Copyright (C) 2011 Gajus Dirkzwager
with changes from Maurizio Loreti that are in the public domain.

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 3 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, <http://www.gnu.org/licenses/>.

On Debian GNU/Linux systems, the complete text of the GNU General
Public License can be found in /usr/share/common-licenses/GPL-3.

GNU Maintainer:  Markus Steinborn <gnugv_maintainer@yahoo.de>

Original Author: Johannes Plass

Contributors:

  - Karl Berry
  - Peter Breitenlohner
  - John Bowman 
  - Bernhard R. Link (brlink@debian.org)
  - José E. Marchesi (jemarch@gnu.org) [was GNU Maintainer]
  - Brett W. McCoy
  - Hans Fredrik Nordhaug
  - Markus Steinborn
  - Thanh Han The <hanthethanh@gmail.com>

Translators:

  - Gajus Dirkzwager <gajus@gmx.com> (Dutch translation)
  - Quentin Gibeaux <quentin.gibeaux@member.fsf.org> (Fremch translation)
  - Cèsar Gil <bluemoon.cgp@gmail.com> (Spanish translation)
  - Marco Paolone <marcopaolone@gmail.com> (Italian translation)

Parts of the usual auto* generated build scripts and parser code
and the compatibility code in lib/ and other places are:
Copyright (C) 1984-2011 Free Software Foundation, Inc,
install-sh is based on code Copyright (C) 1994 X Consortium

This package contains a modified version of the Layout widget, the original is:
* Copyright 1991 Massachusetts Institute of Technology
* Permission to use, copy, modify, distribute, and sell this software and its
* documentation for any purpose is hereby granted without fee, provided that
* the above copyright notice appear in all copies and that both that
* copyright notice and this permission notice appear in supporting
* documentation, and that the name of M.I.T. not be used in advertising or
* publicity pertaining to distribution of the software without specific,
* written prior permission.  M.I.T. makes no representations about the
* suitability of this software for any purpose.  It is provided "as is"
* without express or implied warranty.
*
* M.I.T. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL M.I.T.
* BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
* WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
* OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
* Author:  Keith Packard, MIT X Consortium

